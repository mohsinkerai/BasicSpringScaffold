/**
 * 
 */
package com.example.users;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author Mohsin Mansoor Kerai
 *
 */
public interface UserRepository extends JpaRepository<User, Integer> {

}
